<?php

declare(strict_types=1);

namespace Shop;

use Shop\Warehouse\Contract\IHandler;
use Shop\Warehouse\Handler\CommonHandler;
use Shop\Warehouse\Handler\ConcertTicketsHandler;
use Shop\Warehouse\Handler\MagicHandler;
use Shop\Warehouse\Handler\UniqueHandler;

final class Shop
{
    /**
     * @var Item[]
     */
    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function updateQuality(): void
    {
        $handler = $this->getHandler();

        foreach ($this->items as $item) {
            $handler->handle($item);
        }
    }

    private function getHandler(): IHandler
    {
        $uniqueHandler         = new UniqueHandler();
        $concertTicketsHandler = new ConcertTicketsHandler();
        $magicHandler          = new MagicHandler();
        $commonHandler         = new CommonHandler();
        $uniqueHandler->setNextHandler($concertTicketsHandler)->setNextHandler($magicHandler)->setNextHandler($commonHandler);

        return $uniqueHandler;
    }
}
