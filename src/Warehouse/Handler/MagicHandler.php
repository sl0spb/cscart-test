<?php

declare(strict_types=1);

namespace Shop\Warehouse\Handler;

use Shop\Item;

class MagicHandler extends AHandler
{
    use THandle;

    public const ITEM_MAGIC = 'Magic ';

    protected const STEP_QUALITY_DEFAULT = 2;
    protected const STEP_QUALITY_EXPIRED = 4;

    protected function canHandlerBeUsed(Item $item): bool
    {
        return stripos($item->name, static::ITEM_MAGIC) === 0;
    }
}
