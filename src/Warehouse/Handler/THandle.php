<?php

namespace Shop\Warehouse\Handler;

use Shop\Item;

trait THandle
{
    public function handle(Item $item): void
    {
       $this->canHandlerBeUsed($item) ? $this->updateItem($item) : parent::handle($item);
    }
}
