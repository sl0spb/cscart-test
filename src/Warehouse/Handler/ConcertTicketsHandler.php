<?php

declare(strict_types=1);

namespace Shop\Warehouse\Handler;

use Shop\Item;

class ConcertTicketsHandler extends AHandler
{
    use THandle;

    public const ITEM_CONCERT_TICKETS = 'Concert tickets';
    public const THRESHOLD_DAY        = 10;

    public const MODIFIERS = [
        ['min_day' => 6, 'max_day' => self::THRESHOLD_DAY, 'step' => 2],
        ['min_day' => 1, 'max_day' => 5, 'step' => 3],
    ];

    public function updateItem(Item $item): void
    {
        $step = $this->calculateStep($item);

        $item->quality = min($item->quality + $step, static::MAX_QUALITY);
        $item->sell_in--;
    }

    protected function canHandlerBeUsed(Item $item): bool
    {
        return stripos($item->name, static::ITEM_CONCERT_TICKETS) === 0;
    }

    private function calculateStep(Item $item): int
    {
        if ($item->sell_in > static::THRESHOLD_DAY) {
            return static::STEP_QUALITY_DEFAULT;
        }

        foreach (static::MODIFIERS as $modifier) {
            if ($item->sell_in >= $modifier['min_day'] && $item->sell_in <= $modifier['max_day']) {
                return $modifier['step'];
            }
        }

        return -$item->quality;
    }
}
