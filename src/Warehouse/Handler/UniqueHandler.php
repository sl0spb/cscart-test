<?php

declare(strict_types=1);

namespace Shop\Warehouse\Handler;

use Shop\Item;

class UniqueHandler extends AHandler
{
    use THandle;

    public const ITEM_CHEESE  = 'Blue cheese';
    public const ITEM_MJOLNIR = 'Mjolnir';

    public const MODIFIERS = [
        self::ITEM_CHEESE  => [],
        self::ITEM_MJOLNIR => ['constant_quality' => 80]
    ];

    public function updateItem(Item $item): void
    {
        switch ($item->name) {
            case static::ITEM_CHEESE:
                $step          = $item->sell_in > 0 ? static::STEP_QUALITY_DEFAULT : static::STEP_QUALITY_EXPIRED;
                $item->quality = min($item->quality + $step, static::MAX_QUALITY);
                $item->sell_in--;
                break;
            case static::ITEM_MJOLNIR:
                // If it is impossible that quality contains an invalid value, then this can be removed
                $item->quality = static::MODIFIERS[static::ITEM_MJOLNIR]['constant_quality'];
                break;
            default:
                throw new \UnexpectedValueException('No case for ' . $item->name . ' in ' . __METHOD__);
        }
    }

    protected function canHandlerBeUsed(Item $item): bool
    {
        return array_key_exists($item->name, static::MODIFIERS);
    }
}
