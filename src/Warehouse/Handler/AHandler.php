<?php

declare(strict_types=1);

namespace Shop\Warehouse\Handler;

use Shop\Item;
use Shop\Warehouse\Contract\IHandler;


abstract class AHandler implements IHandler
{
    protected const MAX_QUALITY = 50;
    protected const MIN_QUALITY = 0;

    protected const STEP_QUALITY_DEFAULT = 1;
    protected const STEP_QUALITY_EXPIRED = 2;

    /**
     * @var IHandler
     */
    private $nextHandler;

    public function setNextHandler(IHandler $handler): IHandler
    {
        $this->nextHandler = $handler;

        return $handler;
    }

    public function handle(Item $item): void
    {
        if ($this->nextHandler) {
            $this->nextHandler->handle($item);
        }
    }

    public function updateItem(Item $item): void
    {
        $step = $item->sell_in > 0 ? static::STEP_QUALITY_DEFAULT : static::STEP_QUALITY_EXPIRED;

        $item->quality = max($item->quality - $step, static::MIN_QUALITY);
        $item->sell_in--;
    }

    abstract protected function canHandlerBeUsed(Item $item): bool;
}
