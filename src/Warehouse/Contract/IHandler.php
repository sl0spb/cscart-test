<?php

declare(strict_types=1);

namespace Shop\Warehouse\Contract;

use Shop\Item;

interface IHandler
{
    public function setNextHandler(IHandler $handler): IHandler;

    public function handle(Item $item): void;

    public function updateItem(Item $item): void;
}
