<?php

declare(strict_types=1);

namespace Shop\Warehouse\Handler;

use Shop\Item;

class CommonHandler extends AHandler
{
    use THandle;

    protected function canHandlerBeUsed(Item $item): bool
    {
        return true;
    }
}
